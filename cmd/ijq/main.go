/**
 * File: main.go
 * Written by:  Stephen M. Reaves
 * Created on:  Thu, 16 Jun 2022
 * Description: Interactively build jq stuff
 */

package main

import (
	"log"

	"gitlab.com/reavessm/ijq/pkg/cache"
	"gitlab.com/reavessm/ijq/pkg/fyne"
)

func main() {
	//setCheatText()

	if err := run(); err != nil {
		log.Fatalln("Error!", err)
	}
}

func setCheatText() {
	log.Println("Downloading documentation")
	// go func() {
	// 	resp, err := http.Get("https://gitlab.com/reavessm/ijq/-/raw/main/static/jq-simple.txt")
	// 	if err != nil {
	// 		log.Println(err)
	// 	}
	// 	defer resp.Body.Close()
	//
	// 	simple, err := ioutil.ReadAll(resp.Body)
	// 	if err != nil {
	// 		log.Println(err)
	// 	}
	//
	// 	resp, err = http.Get("https://gitlab.com/reavessm/ijq/-/raw/main/static/jq-adv.txt")
	// 	if err != nil {
	// 		log.Println(err)
	// 	}
	//
	// 	adv, err := ioutil.ReadAll(resp.Body)
	// 	if err != nil {
	// 		log.Println(err)
	// 	}
	//
	// 	fyne.CheatSheetTextSimple = string(simple)
	// 	fyne.CheatSheetTextAdvanced = string(adv)
	//
	// 	log.Println("Done setting documentation")
	// }()
}

func run() error {
	a := fyne.NewApp()
	w := fyne.NewMainWindow(a)
	s := fyne.NewCheatSheetWindow(a)
	log.Println(s.Content().Visible())
	urlBar := fyne.NewURLBar("https://catfact.ninja/fact")
	formatBar := fyne.NewFormatBar(".fact")
	outputBox := fyne.NewOutputBox()

	cheatSheetBox := fyne.NewOutputBox()
	cheatSheetBox.SetText(fyne.CheatSheetTextSimple)

	c := &cache.Http{}

	runBtn := fyne.NewRunButton(urlBar, formatBar, outputBox, c)
	clearBtn := fyne.NewClearScreenButton(outputBox)
	clearCacheBtn := fyne.NewClearCacheButton(c)
	cheatSheetBtn := fyne.NewCheatSheetButton(s)
	cheatSheetToggleBtn := fyne.NewCheatSheetToggleButton(cheatSheetBox)

	buttonBox := fyne.DefaultButtonBox(runBtn, clearBtn, clearCacheBtn, cheatSheetBtn)

	content := fyne.DefaultLayout(
		urlBar,
		formatBar,
		outputBox,
		buttonBox)

	w.SetContent(content)

	s.SetContent(fyne.CheatSheetLayout(cheatSheetBox, cheatSheetToggleBtn))

	w.ShowAndRun()

	return nil
}
