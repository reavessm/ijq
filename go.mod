module gitlab.com/reavessm/ijq

go 1.16

require (
	fyne.io/fyne/v2 v2.2.1
	github.com/itchyny/gojq v0.12.8
)
