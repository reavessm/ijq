DEFAULT:
	run


build:
	go build -o ijq cmd/ijq/main.go

.PHONY: build

run:
	go run cmd/ijq/main.go

update-docs:
	bash -c update-docs.sh



	# jq --help > pkg/fyne/static/jq-simple.txt
	# curl https://raw.githubusercontent.com/stedolan/jq/master/jq.1.prebuilt | pandoc -f man -t plain -o pkg/fyne/static/jq-adv.txt
