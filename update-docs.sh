#!/bin/bash

echo -e "package fyne\n\nconst CheatSheetTextSimple string = \`$(jq --help | sed 's/`/"/g')\`" > pkg/fyne/jq-simple.go
echo -e "package fyne\n\nconst CheatSheetTextAdvanced string = \`$(curl https://raw.githubusercontent.com/stedolan/jq/master/jq.1.prebuilt | pandoc -f man -t plain -o -)\`" > pkg/fyne/jq-adv.go




	
