package fyne

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

func DefaultButtonBox(runBtn, clearScreenBtn, clearCacheBtn, cheatsheetBtn *widget.Button) *fyne.Container {
	return container.New(layout.NewHBoxLayout(),
		cheatsheetBtn,
		layout.NewSpacer(),
		clearCacheBtn,
		clearScreenBtn,
		runBtn)
}

func DefaultLayout(urlBar, formatBar, outputBox *widget.Entry, buttonBox *fyne.Container) *fyne.Container {
	return container.New(layout.NewVBoxLayout(),
		urlBar,
		formatBar,
		outputBox,
		buttonBox)
}

func CheatSheetButtonBox(btn *widget.Button) *fyne.Container {
	return container.New(layout.NewHBoxLayout(),
		layout.NewSpacer(),
		btn)
}

func CheatSheetLayout(outputBox *widget.Entry, btn *widget.Button) *fyne.Container {
	b := CheatSheetButtonBox(btn)
	c :=
		container.New(
			layout.NewBorderLayout(nil, b, nil, nil),
			container.NewVScroll(outputBox),
			b,
		)

	return c
}
