package fyne

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
	"github.com/itchyny/gojq"
	"gitlab.com/reavessm/ijq/pkg/cache"
)

func RunButtonFunc(urlBar, formatBar, outputBox *widget.Entry, c *cache.Http) func() {
	return func() {
		log.Println("Button pressed")

		log.Println("Parsing")
		query, err := gojq.Parse(formatBar.Text)
		if err != nil {
			outputBox.SetText(err.Error())
			return
		}

		url := urlBar.Text

		if string(c.Data) == "" || c.URL != url {
			log.Println("Getting")
			resp, err := http.Get(url)
			if err != nil {
				outputBox.SetText(err.Error())
				return
			}
			defer resp.Body.Close()

			log.Println("Reading")
			data, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				outputBox.SetText(err.Error())
				return
			}

			c.Data = data
			c.URL = url
		}

		input := make(map[string]interface{})

		err = json.Unmarshal(c.Data, &input)
		if err != nil {
			outputBox.SetText(err.Error())
			return
		}

		log.Println("Running")
		iter := query.Run(input)

		for {
			v, ok := iter.Next()
			if !ok {
				break
			}

			if err, ok := v.(error); ok {
				outputBox.SetText(err.Error())
				return
			}

			pretty, err := json.MarshalIndent(v, "", " ")
			if err != nil {
				outputBox.SetText(err.Error())
				return
			}

			outputBox.SetText(outputBox.Text + fmt.Sprintln(string(pretty)))

			// Scroll to bottom
			outputBox.CursorRow = int(outputBox.Size().Height + 1)
		}
	}
}

func ClearScreenButtonFunc(outputBox *widget.Entry) func() {
	return func() {
		outputBox.SetText("")
	}
}

func ClearCacheButtonFunc(c *cache.Http) func() {
	return func() {
		c.Data = []byte{}
	}
}

func CheatSheetButtonFunc(w fyne.Window) func() {
	visible := false
	return func() {
		if visible {
			w.Hide()
			visible = false
		} else {
			w.Show()
			visible = true
		}
	}
}

func CheatSheetToggleButtonFunc(e *widget.Entry) func() {
	advanced := false
	return func() {
		if advanced {
			e.SetText(CheatSheetTextSimple)
			advanced = false
		} else {
			e.SetText(CheatSheetTextAdvanced)
			advanced = true
		}
	}
}
