package fyne

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
)

func NewWindow(a fyne.App, name string) fyne.Window {
	w := a.NewWindow(name)
	w.Resize(fyne.Size{Width: 600, Height: 400})
	return w
}

func NewMainWindow(a fyne.App) fyne.Window {
	w := NewWindow(a, "Main")
	w.SetMaster()
	return w
}

func NewCheatSheetWindow(a fyne.App) fyne.Window {
	w := NewWindow(a, "CheatSheet")
	w.Hide()

	w.SetCloseIntercept(func() {
		w.Hide()
	})

	return w
}

func NewApp() fyne.App {
	return app.New()
}
