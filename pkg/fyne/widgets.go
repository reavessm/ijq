package fyne

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/reavessm/ijq/pkg/cache"
)

func NewURLBar(defaultText string) *widget.Entry {
	u := widget.NewEntry()
	u.SetText(defaultText)
	u.SetPlaceHolder("Please enter api url")

	return u
}

func NewFormatBar(defaultText string) *widget.Entry {
	f := widget.NewEntry()
	f.SetText(defaultText)
	f.SetPlaceHolder("Please enter jq format string")

	return f
}

func NewOutputBox() *widget.Entry {
	o := widget.NewMultiLineEntry()
	o.Disable()
	o.Wrapping = fyne.TextWrapWord
	o.SetMinRowsVisible(10)

	return o
}

func NewRunButton(urlBar, formatBar, outputBox *widget.Entry, c *cache.Http) *widget.Button {
	return widget.NewButton("Run", RunButtonFunc(urlBar, formatBar, outputBox, c))
}

func NewClearScreenButton(ouputBox *widget.Entry) *widget.Button {
	return widget.NewButton("Clear Screen", ClearScreenButtonFunc(ouputBox))
}

func NewClearCacheButton(c *cache.Http) *widget.Button {
	return widget.NewButton("Clear Cache", ClearCacheButtonFunc(c))
}

func NewCheatSheetButton(w fyne.Window) *widget.Button {
	return widget.NewButton("Cheat Sheet", CheatSheetButtonFunc(w))
}

func NewCheatSheetToggleButton(e *widget.Entry) *widget.Button {
	return widget.NewButton("Simple/Advanced", CheatSheetToggleButtonFunc(e))
}
