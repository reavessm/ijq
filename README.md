<!--
File: README.md
Written by: Stephen M. Reaves
Created on: Thu, 16 Jun 2022
-->

# ijq

The Interactive Json Querier (ijq) is a desktop application that allows you to
build jq queries interactively and see the output as you go.

## Usage

To use ijq, simply type in a url in the top box, then you work on your jq query
in the second box.  After you hit 'Run', the output (including errors) will show
up in the third box.  The output is cached BEFORE the filter, so you can keep
changing the query without making multiple HTTP calls.  If you want fresh data,
simply hit the 'Clear Cache' button, then 'Run'.

Here is a Demo:

![random user demo](resources/randomUserDemo.mp4)

## Installation

To install, simply run `go install gitlab.com/reavessm/ijq/cmd/ijq@latest`

### Dependancies

This project uses [fyne](https://fyne.io) for the gui and
[gojq](https://github.com/itchyny/gojq) to do the actual parsing.  The code is
written in Go.
